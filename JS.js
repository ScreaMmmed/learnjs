function getListOfEmployees(array){
  //создание пункта списка, в котором будут данные работника.
  const createEmployeeRow = () => {
    let li = document.createElement("li");
    li.classList.add("employeeRow");
    return li; 
  }
  // создание инпута для формы ввода нового работника.
  const createFormInput = () => {
    let input = document.createElement("input");
    return input;
  }
  // создаем кнопку для добавления данных работника в список
  function createButton(){
    let button = document.createElement("button");
    button.textContent = "add employee";
    button.addEventListener("click",function(){
      let input1 = document.querySelector(".item1");
      let input2 = document.querySelector(".item2");
      let input3 = document.querySelector(".item3");
      ListConstructor([{name:input1.value,age:input2.value,salary:input3.value}])
      input1.value="", input2.value="", input3.value="";
    })
    return button;
  }
  //создание инпута для редактирования пункта списка
  const createEditArea = (value,node,callback) => {
    let input = document.createElement("input");
    input.classList.add("editarea");
    input.size = 32;
    input.value = value;
    input.addEventListener("change", function(){
      node.textContent = this.value;
      node.addEventListener("click",callback)
    })
    return input
  }
  //создание ссылки на удаление строки
  const createDeleteLink = () => {
    let a = document.createElement("a");
    a.href = "#";
    a.textContent = "delete employee";
    a.addEventListener("click",function(event){
      event.preventDefault();
      this.closest(".employeeRow").remove();
    })
    return a;
  }
  //конструктор формы для добавления данных о новом пользователе в список
  function formConstructor(){
    let objectKeys = Object.keys(array[0]);
    for(let i=1;i<=objectKeys.length;i++){
      let input = createFormInput();
      input.classList.add(`item${i}`);
      formContainer.append(input);
    }
    formContainer.append(createButton())
  }
  //конструктор списка работников
  function ListConstructor(ObjectsArray){
    ObjectsArray.forEach((EmployeeInfoObject) => {
      let  employeeRow = createEmployeeRow();
      employeeRow.innerHTML = `<span class="infospan">Name: ${EmployeeInfoObject.name} age: ${EmployeeInfoObject.age} salary: ${EmployeeInfoObject.salary} </span>`;
      EmployeesList.append(employeeRow);
      let span = employeeRow.querySelector(".infospan");
      span.addEventListener("click",function func(){
        let input = createEditArea(span.textContent,span,func);
        this.textContent = "";
        this.append(input);
        this.removeEventListener("click", func);
      })
      employeeRow.append(createDeleteLink());
    })
  }

  let ListContainer = document.querySelector(".ulContainer");
  let EmployeesList = document.createElement("ul");
  ListContainer.append(EmployeesList);
  ListConstructor(array);
  let formContainer = document.querySelector(".formContainer");
  formConstructor();
}

let employees = [
	{name: 'employee1', age: 30, salary: 400}, 
	{name: 'employee2', age: 31, salary: 500}, 
	{name: 'employee3', age: 32, salary: 600}, 
];
getListOfEmployees(employees);